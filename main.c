#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "conio21/conio2.h"

#define DEBUG -1

#define ASCII_ENTER   13     // ENTER
#define ASCII_BACKSPC 8      // backspace
#define ASCII_SPC     32     // space
#define ASCII_EXT     224
#define ASCII_INS     82     // insert
#define ASCII_DEL     83     // delete
#define ASCII_LEFT    75     // seta esquerda
#define ASCII_RIGHT   77     // seta direita
#define ASCII_ESCAPE  27

#define MASK_TEXT_UP     'A'
#define MASK_TEXT_UP_END 'Z'
#define MASK_TEXT_LO     'a'
#define MASK_TEXT_LO_END 'z'
#define MASK_NUMBER1     '0'
#define MASK_NUMBER2     '9'

#define TRUE      -1
#define FALSE     0

/*
    Funcao: accept
    Entrada:
        mask - String contendo o formato que serah aceito pela funcao
        linha, coluna - coordenadas na tela onde os dados serao solicitados
        variavel - endereco que receberah os dados colhidos (char*)
                Este campo eh do tipo ponteiro pois representa
                um endere�o que aponta para uma string
*/
int accept(char* mask , int linha , int coluna , char** variavel){

    int letter;

    int insert_on    = FALSE;
    int letter_parse = FALSE;
    int l=linha,c=coluna;

    int cursor       = 0;
    int cursor_max   = strlen(mask)-1;


    char* buff = (char*)malloc(sizeof(char)*strlen(mask)+1);
    memset(buff,'\0',sizeof(char)*strlen(mask)+1);

    // loop principal ateh que o Enter seja pressionado
    do{
        gotoxy(c+cursor, l);

        letter = getch();

        // verifica se o byte lido eh do ascii extendido
        if(letter==ASCII_EXT) {
            letter = getch();
        }

        #if(DEBUG)
            gotoxy(1, 2);
            printf("Code of letter: %5d",letter);
            gotoxy(c+cursor, l);
        #endif


        switch(letter){

            case ASCII_ENTER:{

                buff[cursor]='\0';
                break;
            }
            case ASCII_BACKSPC:{

                if(cursor>0) cursor--;
                buff[cursor]=' ';

                break;
            }
            case ASCII_SPC:{
                if(insert_on){
                    buff[cursor]=' ';
                }
                else{
                    memcpy(buff+sizeof(char)*(cursor+1),buff+sizeof(char)*(cursor),sizeof(char)*(cursor_max-cursor));
                    buff[cursor]=' ';
                }
                if(cursor<cursor_max) cursor++;
                break;
            }
            case ASCII_INS:{
                insert_on = !insert_on;
                break;
            }
            case ASCII_DEL:{
                buff[cursor]=' ';
                if(cursor<=cursor_max){
                    memcpy(buff+sizeof(char)*cursor,buff+sizeof(char)*(cursor+1),sizeof(char)*(cursor_max-cursor+1));
                    memset(buff+cursor_max,' ',sizeof(char));
                }
                break;
            }
            case ASCII_RIGHT:{
                if(cursor>=0 && cursor<cursor_max) cursor++;
                break;
            }
            case ASCII_LEFT:{
                if(cursor>0 && cursor<=cursor_max) cursor--;
                break;
            }
            default:{

                // verifica se o caractere pressionado faz parte da mask
                if(mask[cursor]==MASK_NUMBER1||mask[cursor]==MASK_NUMBER2){
                    if(letter>=MASK_NUMBER1 && letter<=MASK_NUMBER2){
                        letter_parse = TRUE;
                    }
                }else if(mask[cursor]==MASK_TEXT_UP||mask[cursor]==MASK_TEXT_UP_END){
                    if(letter>=MASK_TEXT_UP && letter<=MASK_TEXT_UP_END){
                        letter_parse = TRUE;
                    }
                }else if(mask[cursor]==MASK_TEXT_LO||mask[cursor]==MASK_TEXT_LO_END){
                    if(letter>=MASK_TEXT_LO && letter<=MASK_TEXT_LO_END){
                        letter_parse = TRUE;
                    }
                }else if(mask[cursor]==letter){
                    letter_parse = TRUE;
                }

                if(letter_parse){

                    if(!insert_on){

                        memcpy(buff+sizeof(char)*(cursor+1),buff+sizeof(char)*(cursor),sizeof(char)*(cursor_max-cursor));

                    }
                    buff[cursor]=letter;
                    if(cursor<cursor_max) cursor++;
                }

            }
        }

        cputsxy(c,l,buff);

    }while(letter != ASCII_ENTER);

    *variavel = buff;

    return 0;
}
void title(){

    clrscr();printf("Hello world of acceptation!");
}
int main()
{
    char mask[32];
    int line,column;
    char* out = NULL;


    do{
        // limpa a tela e move o cursor para a posicao (1,1)
        title();

        // seleciona parametros
        cputsxy(1,2,"Escreva a mascara (Ex.: 'AAA9999', '9999-999' ): ");
        memset(mask,'\0',32);
        gets(mask);

        title();

        cputsxy(1,2,"Digite a linha do cursor: ");
        scanf("%d", &line);

        title();

        cputsxy(1,2,"Digite a coluna do cursor: ");
        scanf("%d", &column);

        title();

        gotoxy(30,1);
        printf(" - Mask: %s | Linha: %d | Coluna: %d", mask,line,column);
        cputsxy(1,2,"Iniciando. . . ");
        accept(mask,line, column, &out);

        gotoxy(1,2);
        clreol();
        printf("Texto digitado: %s", out);

        gotoxy(1,line+2);
        printf("Pressione qualquer tecla para continuar ou Esc para sair.");
    }while(getch()!=ASCII_ESCAPE);

    return 0;
}









